package com.craig.adaoenginetestapp;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.craig.adaoengine.ADaoConfiguration;
import com.craig.adaoengine.ADaoCookieEater;
import com.craig.adaoengine.ADaoCookieEaterListener;
import com.craig.adaoengine.ADaoDownloader;
import com.craig.adaoengine.ADaoDownloaderListener;
import com.craig.adaoengine.ADaoForumGroup;
import com.craig.adaoengine.ADaoPostImage;
import com.craig.adaoengine.ADaoPostMaker;
import com.craig.adaoengine.ADaoPostMakerListener;
import com.craig.adaoengine.ADaoThread;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.Adler32;

import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;

public class MainActivity extends AppCompatActivity {
    ADaoDownloader aDaoDownloader = new ADaoDownloader();
    ADaoCookieEater cookieEater = new ADaoCookieEater();
    ADaoPostMaker postMaker = new ADaoPostMaker();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ADaoConfiguration.getInstance().applicationContext = getApplicationContext();

        testHome();
        testThread();
        testForumGroup();

        aDaoDownloader.listener = new ADaoDownloaderListener() {
            @Override
            public void forumGroupsUpdated(Boolean downloadSuccess, ArrayList<ADaoForumGroup> forumGroups, Exception e) {
                assertTrue(downloadSuccess);
                assertTrue(forumGroups.size() == 6);
                assertTrue(forumGroups.get(0).forums.size() > 0);
            }

            @Override
            public void repliesForThreadDownloaded(Boolean downloadSuccess, ArrayList<ADaoThread> replies, Exception e) {
                assertTrue(downloadSuccess);
                assertTrue(replies.size() > 0);
                assertTrue(e == null);
            }

            @Override
            public void threadsForForumDownloaded(Boolean downloadSuccess, ArrayList<ADaoThread> threads, Exception e) {
                assertTrue(downloadSuccess);
                assertTrue(threads.size() > 0);
                assertTrue(e == null);
            }

            @Override
            public void quoteForThreadDownloaded(Boolean downloadSuccess, ADaoThread thread, Exception e) {
                assertTrue(downloadSuccess);
                assertTrue(!thread.content.isEmpty());
                assertTrue(e == null);
            }
        };

        cookieEater.listener = new ADaoCookieEaterListener() {
            @Override
            public void cookieEaterUpdated(Boolean success, Set<String> cookies, Exception e) {
                assertTrue(success);
                assertTrue(cookies != null);
                assertTrue(cookies.size() > 0);
                ADaoConfiguration.getInstance().setCookies(cookies);
                // Uncomment to make a post to server.
//                testPostMaker();
            }
        };

        aDaoDownloader.updateForumGroups(null);
        aDaoDownloader.downloadThreadsForForumID(4, 1);
        aDaoDownloader.downloadRepliesForThreadID(9740708, 1);
        aDaoDownloader.downloadQuoteForThreadID(10105526);
        if (ADaoConfiguration.getInstance().getCookies() != null) {
            testPostMaker();
        } else {
            cookieEater.updateCookies();
        }
    }

    private void testSharedConfiguration() {
        // Without context, the cookie would be null, the user agent would be default "A_DAO_ENGINE"
        HashSet<String> mockCookies = new HashSet<>();
        mockCookies.add("MOCK_COOKIE");
        ADaoConfiguration.getInstance().applicationContext = null;
        ADaoConfiguration.getInstance().setCookies(mockCookies);
        ADaoConfiguration.getInstance().setUserAgent("MOCK_USER_AGENT");
        assertTrue(ADaoConfiguration.getInstance().getCookies() == null);
        assertTrue(ADaoConfiguration.getInstance().getUserAgent().equals("A_DAO_ENGINE"));
        // With application context, the cookie and user agent would be saved.
        ADaoConfiguration.getInstance().applicationContext = getApplicationContext();
        ADaoConfiguration.getInstance().setCookies(mockCookies);
        ADaoConfiguration.getInstance().setUserAgent("MOCK_USER_AGENT");
        assertTrue(ADaoConfiguration.getInstance().getCookies().equals(mockCookies));
        assertTrue(ADaoConfiguration.getInstance().getUserAgent().equals("MOCK_USER_AGENT"));
        Set<String> storedCookies = ADaoConfiguration.getInstance().getCookies();
        Log.d(null, storedCookies.toString());
    }

    private void testPostMaker() {
        // Test ADaoPostMaker.
        ADaoThread postThread = new ADaoThread();
        postThread.name = "肥贼";
        postThread.content = "肥贼测试中";
        postThread.title = "肥贼";
        postThread.email = "肥贼@hotmail.com";
        postMaker.listener = new ADaoPostMakerListener() {
            @Override
            public void postMakerUpdated(Boolean postSuccess, Map<String, List<String>> responseHeaders, String htmlResponse, Exception e) {
                assertTrue(postSuccess);
                assertTrue(responseHeaders != null);
                assertTrue(!htmlResponse.isEmpty());
                assertTrue(e == null);
            }
        };
        Bitmap randomImage = BitmapFactory.decodeResource(getResources(), R.drawable.random);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Boolean success = randomImage.compress(Bitmap.CompressFormat.JPEG, 90, byteArrayOutputStream);

        try {
            // Create a file to write bitmap data.
            File imageFile = new File(getApplicationContext().getCacheDir(), "RandomFile.jpg");
            imageFile.delete();
            if (imageFile.createNewFile()) {
                Log.d(null, "File created");
            }

            // Convert bitmap to byte array.
            Bitmap bitmap = randomImage;
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 0, bos);
            byte[] bitmapData = bos.toByteArray();

            FileOutputStream fos = new FileOutputStream(imageFile);
            fos.write(bitmapData);
            fos.close();


            assertTrue(success);
            ADaoPostImage postImage = new ADaoPostImage();
            postImage.imageFile = imageFile;
            postMaker.reply(postThread, 10088110, postImage);
        } catch (IOException e) {
            e.printStackTrace();
            assertTrue(false);
        }
    }

    private void testForumGroup() {
        try {
            InputStream inputStream = getApplicationContext()
                    .getResources()
                    .openRawResource(getResources().getIdentifier("forums", "raw", getPackageName()));
            byte[] buffer = new byte[inputStream.available()];
            inputStream.read(buffer);
            String jsonString = new String(buffer);
            // Try to construct threads array from home.json.
            JSONArray jsonArray = new JSONArray(jsonString);
            ArrayList<ADaoForumGroup> forumGroups = new ArrayList<>();
            for (int i = 0; i< jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                if (jsonObject != null) {
                    ADaoForumGroup forumGroup= new ADaoForumGroup(jsonObject);
                    forumGroups.add(forumGroup);
                }
            }
            assertTrue(forumGroups.size() == 6);
            Boolean threadsHaveResponses = false;
            for (ADaoForumGroup forumGroup: forumGroups) {
                assertTrue(forumGroup.forums.size() > 0);
            }
        } catch (Exception e) {
            Log.d(null, e.getMessage());
            assertTrue(false);
        }
    }

    void testHome() {
        try {
            InputStream inputStream = getApplicationContext()
                    .getResources()
                    .openRawResource(getResources().getIdentifier("home", "raw", getPackageName()));
            byte[] buffer = new byte[inputStream.available()];
            inputStream.read(buffer);
            String jsonString = new String(buffer);
            // Try to construct threads array from home.json.
            JSONArray jsonArray = new JSONArray(jsonString);
            ArrayList<ADaoThread> threads = new ArrayList<ADaoThread>();
            for (int i = 0; i< jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                if (jsonObject != null) {
                    ADaoThread thread = new ADaoThread(jsonObject);
                    threads.add(thread);
                }
            }
            assertTrue(threads.size() == 20);
            Boolean threadsHaveResponses = false;
            for (ADaoThread thread: threads) {
                if (thread.replies.size() > 0) {
                    threadsHaveResponses = true;
                    break;
                }
            }
            assertTrue(threadsHaveResponses);
        } catch (Exception e) {
            Log.d(null, e.getMessage());
            assertTrue(false);
        }
    }

    void testThread() {
        try {
            InputStream inputStream = getApplicationContext()
                    .getResources()
                    .openRawResource(getResources().getIdentifier("thread", "raw", getPackageName()));
            byte[] buffer = new byte[inputStream.available()];
            inputStream.read(buffer);
            String jsonString = new String(buffer);
            JSONObject jsonObject = new JSONObject(jsonString);
            ADaoThread parentThread = new ADaoThread(jsonObject);
            ArrayList<ADaoThread> replies = parentThread.replies;
            assertTrue(parentThread != null);
            assertTrue(replies.size() > 0);
        } catch (Exception e) {
            Log.d(null, e.getLocalizedMessage());
            assertTrue(false);
        }
    }
}
