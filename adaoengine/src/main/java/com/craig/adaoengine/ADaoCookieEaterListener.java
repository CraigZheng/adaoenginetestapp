package com.craig.adaoengine;

import java.util.Set;

/**
 * Created by Craig on 9/10/16.
 */

public interface ADaoCookieEaterListener {

    /**
     * Always return in a background thread.
     * @param success the result of the update.
     * @param cookies a Set of strings that represent the cookie.
     * @param e Exception.
     */
    public void cookieEaterUpdated(Boolean success, Set<String> cookies, Exception e);

}
