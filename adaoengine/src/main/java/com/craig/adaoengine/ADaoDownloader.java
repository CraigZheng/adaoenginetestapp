package com.craig.adaoengine;

import android.text.TextUtils;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

/**
 * Created by Craig on 29/9/16.
 * This class is responsible for downloading contents from the server.
 */

public class ADaoDownloader {
    final String kForumID = "<kForumID>";
    final String kPageNumber = "<kPageNumber>";
    final String kThreadID = "<kThreadID>";
    final String GET_FORUM_URL_ENDPOINT = "https://h.nimingban.com/Api/getForumList";
    final String GET_THREAD_IN_FORUM_ENDPOINT = "https://h.nimingban.com/Api/showf?id="+kForumID+"&page="+kPageNumber;
    final String GET_CONTENT_FOR_THREAD_END_POINT = "https://h.nimingban.com/Api/thread?id="+kThreadID+"&page=" + kPageNumber;
    final String GET_QUOTE_FOR_THREAD_ENDPOINT = "https://h.nimingban.com/Api/ref/id/" + kThreadID;

    public ADaoDownloaderListener listener;

    private HttpURLConnection updateForumGroupsConnection;
    private HttpURLConnection getThreadsConnection;
    private HttpURLConnection getRepliesConnection;
    private HttpURLConnection getQuoteConnection;

    /**
     * @param targetEndpoint can be null, in which case the default GET_FORUM endpoint would be used.
     */
    public void updateForumGroups(String targetEndpoint) {
        if (targetEndpoint == null) {
            targetEndpoint = GET_FORUM_URL_ENDPOINT;
        }
        final String finalEndpoint = targetEndpoint;
        try {
            // Network connections must be done within a background thread.
            new Thread() {
                @Override
                public void run() {
                    try {
                        URLConnection connection = new URL(finalEndpoint).openConnection();
                        updateForumGroupsConnection = (HttpURLConnection) connection;
                        connection.setRequestProperty(ADaoConfiguration.USER_AGENT, ADaoConfiguration.getInstance().getUserAgent());
                        if (ADaoConfiguration.getInstance().getCookies() != null) {
                            connection.setRequestProperty(ADaoConfiguration.COOKIE,
                                    TextUtils.join(";", ADaoConfiguration.getInstance().getCookies()));
                        }
                        final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

                        String line = "";
                        StringBuilder sb = new StringBuilder();
                        while ((line = bufferedReader.readLine()) != null) {
                            sb.append(line);
                        }
                        bufferedReader.close();
                        String jsonString = sb.toString();
                        Boolean downloadResult = false;
                        ArrayList<ADaoForumGroup> forumGroups = null;
                        if (!jsonString.isEmpty()) {
                            downloadResult = true;
                            JSONArray jsonArray = new JSONArray(jsonString);
                            forumGroups = new ArrayList<>();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                if (jsonObject != null) {
                                    ADaoForumGroup forumGroup = new ADaoForumGroup(jsonObject);
                                    forumGroups.add(forumGroup);
                                }
                            }
                        }
                        if (listener != null) {
                            listener.forumGroupsUpdated(downloadResult, forumGroups, null);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        if (listener != null) {
                            listener.forumGroupsUpdated(false, null, e);
                        }
                    } finally {
                        updateForumGroupsConnection.disconnect();
                    }
                }
            }.start();
        } catch (Exception e) {
            // Too lazy to handle, let listener do its job.
            e.printStackTrace();
            if (listener != null) {
                listener.forumGroupsUpdated(false, null, e);
            }
        }
    }

    /**
     * This method returns in a background thread, it's your responsibility to manage the main thread.
     * @param forumID the ID of the forum.
     * @param page the page number, starting at 1.
     */
    public void downloadThreadsForForumID(final int forumID, final int page) {
        new Thread() {
            @Override
            public void run() {
                try {
                    if (forumID < 0 || page < 0) {
                        Log.v(null, "No forumID or page number given, cannot download.");
                        if (listener != null) {
                            listener.threadsForForumDownloaded(false, null, null);
                        }
                        return;
                    }
                    String targetURLString = GET_THREAD_IN_FORUM_ENDPOINT.replace(kForumID, String.valueOf(forumID))
                            .replace(kPageNumber, String.valueOf(page));
                    getThreadsConnection = (HttpURLConnection) new URL(targetURLString).openConnection();
                    getThreadsConnection.setRequestProperty(ADaoConfiguration.USER_AGENT,
                            ADaoConfiguration.getInstance().getUserAgent());
                    if (ADaoConfiguration.getInstance().getCookies() != null) {
                        getThreadsConnection.setRequestProperty(ADaoConfiguration.COOKIE,
                                TextUtils.join(";", ADaoConfiguration.getInstance().getCookies()));
                    }
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(getThreadsConnection.getInputStream()));
                    StringBuilder stringBuilder = new StringBuilder();
                    String line = "";
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuilder.append(line);
                    }
                    String jsonString = stringBuilder.toString();
                    if (!jsonString.isEmpty()) {
                        JSONArray jsonArray = new JSONArray(jsonString);
                        ArrayList<ADaoThread> threads = new ArrayList<ADaoThread>();
                        for (int i = 0; i< jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            if (jsonObject != null) {
                                ADaoThread thread = new ADaoThread(jsonObject);
                                threads.add(thread);
                            }
                        }
                        // Call listener for the results.
                        if (listener != null) {
                            listener.threadsForForumDownloaded(true, threads, null);
                        }
                    } else {
                        if (listener != null) {
                            listener.threadsForForumDownloaded(false, null, null);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    if (listener != null) {
                        listener.threadsForForumDownloaded(false, null, e);
                    }
                } finally {
                    getThreadsConnection.disconnect();
                }
            }
        }.start();
    }

    /**
     * This method returns in a background thread.
     * @param threadID the ID of the thread that you wish to get response from.
     * @param page the page number, starting at 0.
     */
    public void downloadRepliesForThreadID(final int threadID, final int page) {
        new Thread() {
            @Override
            public void run() {
                try {
                    if (threadID < 0 || page < 0) {
                        Log.v(null, "Cannot get replies with empty threadID or page number!");
                        if (listener != null) {
                            listener.repliesForThreadDownloaded(false, null, null);
                        }
                        return;
                    }
                    String targetURLString = GET_CONTENT_FOR_THREAD_END_POINT.replace(kThreadID, String.valueOf(threadID))
                            .replace(kPageNumber, String.valueOf(page));
                    getRepliesConnection = (HttpURLConnection) new URL(targetURLString).openConnection();
                    getRepliesConnection.setRequestProperty(ADaoConfiguration.USER_AGENT,
                            ADaoConfiguration.getInstance().getUserAgent());
                    if (ADaoConfiguration.getInstance().getCookies() != null) {
                        getRepliesConnection.setRequestProperty(ADaoConfiguration.COOKIE,
                                TextUtils.join(";", ADaoConfiguration.getInstance().getCookies()));
                    }
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(getRepliesConnection.getInputStream()));
                    StringBuilder stringBuilder = new StringBuilder();
                    String line = "";
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuilder.append(line);
                    }
                    String jsonString = stringBuilder.toString();
                    if (!jsonString.isEmpty()) {
                        JSONObject jsonObject= new JSONObject(jsonString);
                        ADaoThread downloadedThread = new ADaoThread(jsonObject);
                        // Response with the downloaded content.
                        if (listener != null) {
                            listener.repliesForThreadDownloaded(true, downloadedThread.replies, null);
                        }
                    } else {
                        // Cannot parse the returned json.
                        if (listener != null) {
                            listener.repliesForThreadDownloaded(false, null, null);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    if (listener != null) {
                        listener.repliesForThreadDownloaded(false, null, e);
                    }
                } finally {
                    getRepliesConnection.disconnect();
                }
            }
        }.start();
    }

    /**
     * This method returns in a background thread.
     * @param threadID the ID of the thread that you wish to get quote from.
     */
    public void downloadQuoteForThreadID(final int threadID) {
        new Thread() {
            @Override
            public void run() {
                try {
                    if (threadID < 0) {
                        Log.v(null, "Cannot get replies with empty threadID or page number!");
                        if (listener != null) {
                            listener.quoteForThreadDownloaded(false, null, null);
                        }
                        return;
                    }
                    String targetURLString = GET_QUOTE_FOR_THREAD_ENDPOINT.replace(kThreadID, String.valueOf(threadID));
                    getQuoteConnection = (HttpURLConnection) new URL(targetURLString).openConnection();
                    getQuoteConnection.setRequestProperty(ADaoConfiguration.USER_AGENT,
                            ADaoConfiguration.getInstance().getUserAgent());
                    if (ADaoConfiguration.getInstance().getCookies() != null) {
                        getQuoteConnection.setRequestProperty(ADaoConfiguration.COOKIE,
                                TextUtils.join(";", ADaoConfiguration.getInstance().getCookies()));
                    }
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(getQuoteConnection.getInputStream()));
                    StringBuilder stringBuilder = new StringBuilder();
                    String line = "";
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuilder.append(line);
                    }
                    String jsonString = stringBuilder.toString();
                    if (!jsonString.isEmpty()) {
                        JSONObject jsonObject= new JSONObject(jsonString);
                        ADaoThread downloadedThread = new ADaoThread(jsonObject);
                        // Response with the downloaded content.
                        if (listener != null) {
                            listener.quoteForThreadDownloaded(true, downloadedThread, null);
                        }
                    } else {
                        // Cannot parse the returned json.
                        if (listener != null) {
                            listener.quoteForThreadDownloaded(false, null, null);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    if (listener != null) {
                        listener.quoteForThreadDownloaded(false, null, e);
                    }
                } finally {
                    getQuoteConnection.disconnect();
                }
            }
        }.start();
    }

    public void stopUpdatingForumGroups() {
        if (updateForumGroupsConnection != null) {
            updateForumGroupsConnection.disconnect();
        }
        updateForumGroupsConnection = null;
    }

    public void stopDownloadingThreads() {
        if (getThreadsConnection != null) {
            getThreadsConnection.disconnect();
        }
        getThreadsConnection = null;
    }

    public void stopDownloadingReplies() {
        if (getRepliesConnection != null) {
            getRepliesConnection.disconnect();
        }
        getRepliesConnection = null;
    }
}
