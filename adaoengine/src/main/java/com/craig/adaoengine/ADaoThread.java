package com.craig.adaoengine;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

/**
 * Created by Craig on 27/9/16.
 */

public class ADaoThread {
    public int responseCount;
    public int ID;
    public String userID;
    public String name;
    public String email;
    public String title;
    public String content;
    public String imageName;
    public boolean sage;
    public boolean admin;
    public String postDate;
    public ArrayList<ADaoThread> replies;

    public ADaoThread() {}; // Empty constructor.

    public ADaoThread(JSONObject jsonObject) throws Exception {
        if (jsonObject != null) {
            try {
                if (jsonObject.has("id")) {
                    ID = jsonObject.getInt("id");
                }
                if (jsonObject.has("img") && jsonObject.has("ext")) {
                    imageName = jsonObject.getString("img") + jsonObject.getString("ext");
                }
                if (jsonObject.has("now")) {
                    postDate = jsonObject.getString("now");
                }
                if (jsonObject.has("userid")) {
                    userID = jsonObject.getString("userid");
                }
                if (jsonObject.has("name")) {
                    name = jsonObject.getString("name");
                }
                if (jsonObject.has("email")) {
                    email = jsonObject.getString("email");
                }
                if (jsonObject.has("title")) {
                    title = jsonObject.getString("title");
                }
                if (jsonObject.has("content")) {
                    content = jsonObject.getString("content");
                }
                if (jsonObject.has("sage")) {
                    sage = jsonObject.getInt("sage") == 1;
                }
                if (jsonObject.has("admin")) {
                    admin = jsonObject.getInt("admin") == 1;
                }
                if (jsonObject.has("replyCount")) {
                    responseCount = jsonObject.getInt("replyCount");
                }
                if (jsonObject.has("replys") && jsonObject.get("replys") instanceof JSONArray) {
                    JSONArray replies = (JSONArray) jsonObject.get("replys");
                    // Construct the replies.
                    if (replies.length() > 0) {
                        this.replies = new ArrayList<ADaoThread>();
                        for (int i = 0; i < replies.length(); i++) {
                            JSONObject subObject = replies.getJSONObject(i);
                            if (subObject != null) {
                                ADaoThread reply = new ADaoThread(subObject);
                                this.replies.add(reply);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                // Let the caller handle this.
                throw e;
            }
        } else {
            throw new Exception("Json cannot be parsed.");
        }
    }
    /*
    {
    "id":"9853552",
            "img":"",
            "ext":"",
            "now":"2016-09-22(四)21:31:57",
            "userid":"Ovear",
            "name":"无名氏",
            "email":"",
            "title":"无标题",
            "content":"海外/移动CDN已上线，海外用户和移动用户访问速度应该会有好转。<br />\r\n请移动、海外用户在本串下反馈~<br />\r\n09/22 22:34 欧洲CDN已上线，请欧洲用户在本串下反馈<br />\r\n如果会使用 ping 命令， 麻烦在本串下 贴一下 ping h.nimingban.com 的结果，并附上自己区域以及运营商<br/>\r\n比如说，广州电信4M<br/>\r\n<font color=\"red\">请目前仍然无法上岛的电脑用户 (如湖南移动) 联系iheretic@扣扣.com，以便我们跟进优化。（请把扣扣替换成qq）</font>",
            "sage":"1",
            "admin":"1",
            "remainReplys":238,
            "replyCount":"243",
            "replys":[]
}
*/
}
