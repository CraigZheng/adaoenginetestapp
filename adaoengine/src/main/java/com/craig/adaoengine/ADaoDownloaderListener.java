package com.craig.adaoengine;

import java.util.ArrayList;

/**
 * Created by Craig on 2/10/16.
 */

public interface ADaoDownloaderListener {

    /**
     * All ADaoDownloader callbacks would return in a background thread.
     * @param downloadSuccess
     * @param forumGroups
     * @param e
     */
    public void forumGroupsUpdated(Boolean downloadSuccess, ArrayList<ADaoForumGroup> forumGroups, Exception e);

    /**
     * All ADaoDownloader callbacks would return in a background thread.
     * @param downloadSuccess
     * @param threads
     * @param e
     */
    public void threadsForForumDownloaded(Boolean downloadSuccess, ArrayList<ADaoThread> threads, Exception e);

    /**
     * All ADaoDownloader callbacks would return in a background thread.
     * @param downloadSuccess
     * @param replies
     * @param e
     */
    public void repliesForThreadDownloaded(Boolean downloadSuccess, ArrayList<ADaoThread> replies, Exception e);

    /**
     * All ADaoDownloader callbacks would return in a background thread.
     * @param downloadSuccess
     * @param thread target thread returned by the server.
     * @param e
     */
    public void quoteForThreadDownloaded(Boolean downloadSuccess, ADaoThread thread, Exception e);

}
