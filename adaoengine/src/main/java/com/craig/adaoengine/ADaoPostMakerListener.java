package com.craig.adaoengine;

import java.util.List;
import java.util.Map;

/**
 * Created by Craig on 9/10/16.
 */

public interface ADaoPostMakerListener {

    /**
     * ADaoPostMaker always return in a background thread.
     * @param postSuccess the server has received the post, but it does not guarantee that the posting is successful, you will have to check the htmlResponse to make sure of that.
     * @param responseHeaders the response headers, might contain a usable cookie.
     * @param htmlResponse the html response, you must check it to make sure the posting is successful.
     * @param e Exception.
     */
    void postMakerUpdated(Boolean postSuccess, Map<String, List<String>> responseHeaders, String htmlResponse, Exception e);

}
