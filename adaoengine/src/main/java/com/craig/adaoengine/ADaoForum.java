package com.craig.adaoengine;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * Created by Craig on 27/9/16.
 */

public class ADaoForum {
    public int ID;
    public int fgroup;
    public int sort;
    public String name;
    public String showName;
    public String message;
    public int interval;
    public Date createdAt;
    public Date updatedAt;
    public String status;

    public ADaoForum(JSONObject jsonObject) throws JSONException {
        if (jsonObject != null) {
            try {
                if (jsonObject.has("id")) {
                    ID = jsonObject.getInt("id");
                }
                if (jsonObject.has("fgroup")) {
                    fgroup = jsonObject.getInt("fgroup");
                }
                if (jsonObject.has("sort")) {
                    sort = jsonObject.getInt("sort");
                }
                if (jsonObject.has("name")) {
                    name = jsonObject.getString("name");
                }
                if (jsonObject.has("showName")) {
                    showName = jsonObject.getString("showName");
                }
                if (jsonObject.has("msg")) {
                    message = jsonObject.getString("msg");
                }
                if (jsonObject.has("interval")) {
                    interval = jsonObject.getInt("interval");
                }
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd KK:ss:ss");
                if (jsonObject.has("createdAt")) {
                    try {
                        createdAt = dateFormat.parse(jsonObject.getString("createdAt"));
                    } catch (Exception e) {}
                }
                if (jsonObject.has("updateAt")) {
                    try {
                        updatedAt = dateFormat.parse(jsonObject.getString("updateAt"));
                    } catch (Exception e) {}
                }
                if (jsonObject.has("status")) {
                    status = jsonObject.getString("status");
                }
            } catch (JSONException jsonException) {
                throw jsonException;
            }
        }
    }
}

/*
{
        "id": "18",
        "fgroup": "6",
        "sort": "1",
        "name": "值班室",
        "showName": "",
        "msg": "<p>&bull;本版发文间隔为15秒。<br />\r\n&bull;请在此举报不良内容，并附上串地址以及发言者ID。如果是回复串，请附上&ldquo;回应&rdquo;链接的地址，格式为&gt;&gt;No.串ID或&gt;&gt;No.回复ID<br />\r\n&bull;主站相关问题反馈、建议请在这里留言<br />\r\n&bull;已处理的举报将SAGE。</p>\r\n",
        "interval": "15",
        "createdAt": "2011-09-30 23:55:20",
        "updateAt": "2015-07-26 15:39:24",
        "status": "n"
      }
*/

