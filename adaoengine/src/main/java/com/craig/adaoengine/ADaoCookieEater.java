package com.craig.adaoengine;

import android.annotation.SuppressLint;
import android.provider.Settings;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Craig on 9/10/16.
 * Used to get a cookie from the server.
 */

public class ADaoCookieEater {

    final String GET_COOKIE_ENDPOINT = "https://h.nimingban.com/Home/Api/getCookie?deviceid=";

    public ADaoCookieEaterListener listener;

    /**
     * Update cookies. Cookies are NOT automatically save, you will have to save the cookies in your listener object.
     */
    public void updateCookies() {
        new Thread() {
            @Override
            public void run() {
                try {
                    String deviceID;
                    if (ADaoConfiguration.getInstance().applicationContext != null) {
                        deviceID = Settings.Secure.getString(ADaoConfiguration.getInstance().applicationContext.getContentResolver(),
                                        Settings.Secure.ANDROID_ID);
                    } else {
                        deviceID = "No Device ID";
                    }
                    HttpURLConnection connection = (HttpURLConnection) new URL(GET_COOKIE_ENDPOINT + deviceID).openConnection();
                    connection.setRequestProperty(ADaoConfiguration.USER_AGENT, ADaoConfiguration.getInstance().getUserAgent());
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    String line = "";
                    StringBuilder sb = new StringBuilder();
                    while ((line = bufferedReader.readLine()) != null) {
                        sb.append(line);
                    }
                    // Shamelessly copied from http://stackoverflow.com/questions/16150089/how-to-handle-cookies-in-httpurlconnection-using-cookiemanager
                    // And modified to suit my needs.
                    Map<String, List<String>> headerFields = connection.getHeaderFields();
                    List<String> cookiesHeader = headerFields.get("Set-Cookie");
                    // Get the list of values in Set-Cookie and return it.
                    if (cookiesHeader != null && cookiesHeader.size() > 0) {
                        notifyListener(sb.toString().toLowerCase().contains("ok"),
                                new HashSet<>(cookiesHeader),
                                null);
                    } else {
                        // Empty cookie set, return false.
                        notifyListener(false, null, null);
                    }
                } catch (Exception e) {
                    notifyListener(false, null, e);
                }
            }
        }.start();
    }

    private void notifyListener(Boolean success, Set<String> cookies, Exception e) {
        if (e != null) {
            e.printStackTrace();
        }
        if (listener != null) {
            listener.cookieEaterUpdated(success, cookies, e);
        }
    }
}
