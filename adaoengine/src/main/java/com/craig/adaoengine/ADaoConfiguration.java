package com.craig.adaoengine;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.Set;
import java.util.logging.Logger;

import static junit.framework.Assert.assertTrue;

/**
 * Created by Craig on 29/9/16.
 * You can set your custom cookies and user agent in this singleton class.
 */
public class ADaoConfiguration {
    private static String A_DAO_ENGINE = "A_DAO_ENGINE";
    static String COOKIE = "Cookie";
    static String USER_AGENT = "User-Agent";

    public Context applicationContext;

    private static ADaoConfiguration ourInstance = new ADaoConfiguration();

    public static ADaoConfiguration getInstance() {
        return ourInstance;
    }

    private ADaoConfiguration() { }

    /*
    Get stored cookies, if a cookie cannot be found, this method would return null.
     */
    public Set<String> getCookies() {
        Set<String> cookies = null;
        if (sharedPreferences() != null) {
            cookies = sharedPreferences().getStringSet(COOKIE, null);
        }
        return cookies;
    }

    /*
    Set cookies to store, cookies would be stored in SharedPreferences with "Cookie" as the key.
     */
    public void setCookies(Set<String> cookies) {
        if (sharedPreferences() != null) {
            sharedPreferences().edit().putStringSet(COOKIE, cookies).apply();
        } else {
            Log.v("ADaoConfiguration", "Cannot get SharedPreferences, please provide an applicationContext.");
        }
    }

    /*
    If you've never set a user agent, "A_DAO_ENGINE" would be returned.
     */
    public String getUserAgent() {
        String userAgent = A_DAO_ENGINE;
        if (sharedPreferences() != null) {
            userAgent = sharedPreferences().getString(USER_AGENT, A_DAO_ENGINE);
        }
        return userAgent;
    }

    /*
    Set your user agent.
     */
    public void setUserAgent(String userAgent) {
        if (sharedPreferences() != null) {
            sharedPreferences().edit().putString(USER_AGENT, userAgent).apply();
        } else {
            Log.v("ADaoConfiguration", "Cannot get SharedPreferences, please provide an applicationContext.");
        }
    }

    private SharedPreferences sharedPreferences() {
        if (applicationContext != null) {
            return applicationContext.getSharedPreferences(A_DAO_ENGINE, Context.MODE_PRIVATE);
        } else {
            return null;
        }
    }

}
