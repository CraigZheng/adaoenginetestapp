package com.craig.adaoengine;

import android.text.TextUtils;
import android.util.Pair;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Craig on 9/10/16.
 * You can post contents to the server with this class.
 */

public class ADaoPostMaker {

    final String POST_NEW_ENDPOINT = "https://h.nimingban.com/Home/Forum/doPostThread.html";
    final String REPLY_ENDPOINT = "https://h.nimingban.com/Home/Forum/doReplyThread.html";
    private final String UTF8 = "UTF-8";

    public ADaoPostMakerListener listener;
    private HttpURLConnection newPostConnection;
    private HttpURLConnection replyConnection;

    /**
     * Make a new post to the specified forum.
     * @param thread an ADaoThread object that contains the message that you want to send.
     * @param forumID forumID of the target forum.
     * @param postImage an ADaoPostImage object that contains the image file and some extra settings, currently only jpg and gif formats are supported.
     */
    public void newPost(ADaoThread thread, Integer forumID, ADaoPostImage postImage) {
        postContent(thread, forumID, true, postImage);
    }

    /**
     * Reply to a specified thread.
     * @param thread an ADaoThread object that contains the message that you want to send.
     * @param replyToID thread ID of the target thread.
     * @param postImage an ADaoThread object that contains the message that you want to send.
     */
    public void reply(ADaoThread thread, Integer replyToID, ADaoPostImage postImage) {
        postContent(thread, replyToID, false, postImage);
    }

    /**
     * Stop whatever that is currently in progress.
     */
    public void stopPosting() {
        if (newPostConnection != null) {
            newPostConnection.disconnect();
            newPostConnection = null;
        }
        if (replyConnection != null) {
            replyConnection.disconnect();
            replyConnection = null;
        }
    }

    private void notifyListener(Boolean postSuccess, Map<String, List<String>> responseHeaders, String htmlResponse, Exception e) {
        if (e != null) {
            e.printStackTrace();
        }
        if (listener != null) {
            listener.postMakerUpdated(postSuccess, responseHeaders, htmlResponse, e);
        }
    }

    private void postContent(final ADaoThread thread, final Integer ID, final Boolean isNewThread, final ADaoPostImage postImage) {
        new Thread() {
            @Override
            public void run() {
                try {
                    if (thread == null || ID < 0) {
                        throw new Exception("Insufficient parameters, cannot post content.");
                    }
                    if (postImage != null && postImage.imageFile == null) {
                        throw new Exception("Image parameter incorrect: image file empty.");
                    }
                    // Set header properties.
                    List<Pair<String, String>> headerFields = new ArrayList<>();
                    headerFields.add(new Pair<String, String>(ADaoConfiguration.USER_AGENT, ADaoConfiguration.getInstance().getUserAgent()));
                    if (ADaoConfiguration.getInstance().getCookies() != null) {
                        headerFields.add(new Pair<String, String>(ADaoConfiguration.COOKIE,
                                TextUtils.join(";", ADaoConfiguration.getInstance().getCookies())));
                    }
                    MultipartUtility multipart = new MultipartUtility(isNewThread ? POST_NEW_ENDPOINT : REPLY_ENDPOINT, UTF8, headerFields);
                    if (isNewThread) {
                        newPostConnection = multipart.httpConn;
                    } else {
                        replyConnection = multipart.httpConn;
                    }
                    // Add fields, each with corresponding name.
                    multipart.addFormField(isNewThread ? "fid" : "resto", ID.toString());
                    if (thread.name != null) {
                        multipart.addFormField("name", thread.name);
                    }
                    if (thread.title != null) {
                        multipart.addFormField("title", thread.title);
                    }
                    if (thread.email != null) {
                        multipart.addFormField("email", thread.email);
                    }
                    if (thread.content != null) {
                        multipart.addFormField("content", thread.content);
                    }
                    if (postImage != null) {
                        multipart.addFormField("water", postImage.shouldWaterMark ? "true" : "false");
                        multipart.addFilePart("image", postImage.imageFile);
                    }
                    multipart.finish();
                    notifyListener(true, multipart.responseHeaders, multipart.htmlResponse, null);
                } catch (Exception e) {
                    notifyListener(false, null, null, e);
                }
            }
        }.start();
    }
}
