package com.craig.adaoengine;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Craig on 29/9/16.
 * A class that wraps an array of ADaoForum objects, can be used to group forums to different category.
 */

public class ADaoForumGroup {
    public int ID;
    public int sort;
    public String name;
    public String status;
    public ArrayList<ADaoForum> forums;

    public ADaoForumGroup(JSONObject jsonObject) throws JSONException {
        if (jsonObject != null) {
            try {
                if (jsonObject.has("id")) {
                    ID = jsonObject.getInt("id");
                }
                if (jsonObject.has("sort")) {
                    sort = jsonObject.getInt("sort");
                }
                if (jsonObject.has("name")) {
                    name = jsonObject.getString("name");
                }
                if (jsonObject.has("status")) {
                    status = jsonObject.getString("status");
                }
                if (jsonObject.has("forums") && jsonObject.get("forums") instanceof JSONArray) {
                    JSONArray forumsArray = jsonObject.getJSONArray("forums");
                    forums = new ArrayList<>();
                    for (int i = 0; i < forumsArray.length(); i++) {
                        JSONObject forumsObject = forumsArray.getJSONObject(i);
                        if (forumsObject != null) {
                            forums.add(new ADaoForum(forumsObject));
                        }
                    }
                }
            } catch (JSONException jsonException) {
                // Let the caller worry about the json exception.
                throw jsonException;
            }
        }
    }
}

/*
{
    "id": "6",
    "sort": "6",
    "name": "管理",
    "status": "n",
    "forums": [
      {
        "id": "18",
        "fgroup": "6",
        "sort": "1",
        "name": "值班室",
        "showName": "",
        "msg": "<p>&bull;本版发文间隔为15秒。<br />\r\n&bull;请在此举报不良内容，并附上串地址以及发言者ID。如果是回复串，请附上&ldquo;回应&rdquo;链接的地址，格式为&gt;&gt;No.串ID或&gt;&gt;No.回复ID<br />\r\n&bull;主站相关问题反馈、建议请在这里留言<br />\r\n&bull;已处理的举报将SAGE。</p>\r\n",
        "interval": "15",
        "createdAt": "2011-09-30 23:55:20",
        "updateAt": "2015-07-26 15:39:24",
        "status": "n"
      }
    ]
  }
 */