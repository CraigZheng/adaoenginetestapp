package com.craig.adaoengine;

import java.io.File;

/**
 * Created by Craig on 11/10/16.
 */
public class ADaoPostImage {

    /**
     * @param imageFile should not and cannot be null, the app would crash if you want to send a null image file, currently supports only jpg and gif formats.
     */
    public File imageFile; // Cannot be null.
    public Boolean shouldWaterMark = false; // Default is false.

}
